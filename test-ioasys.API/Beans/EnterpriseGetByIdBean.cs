﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test_ioasys.Model.Entities;

namespace test_ioasys.API.Beans
{
    public class EnterpriseGetByIdBean
    {
        public EnterpriseGetByIdBean(Enterprise enterprise)
        {
            this.enterprise = enterprise;
        }

        public Enterprise enterprise;
    }
}
