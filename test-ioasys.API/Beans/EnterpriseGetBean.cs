﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using test_ioasys.Model.Entities;

namespace test_ioasys.API.Beans
{
    public class EnterpriseGetBean
    {
        public EnterpriseGetBean(IQueryable<Enterprise> enterprises)
        {
            this.enterprises = enterprises;
        }

        public IQueryable<Enterprise> enterprises;
    }
}
