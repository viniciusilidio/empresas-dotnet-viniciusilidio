﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using test_ioasys.Data;
using test_ioasys.Data.Interfaces;
using test_ioasys.Model.DatabaseSetup;
using test_ioasys.Service;
using test_ioasys.Service.Interfaces;
using Microsoft.EntityFrameworkCore.Proxies;


namespace test_ioasys.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IEnterpriseService, EnterpriseService>();
            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();
    
            services.AddEntityFrameworkSqlServer()
               .AddDbContext<TestIoasysContext>(
                   options => options
                   .UseSqlServer(
                       Configuration["ConnectionString"],
                       builder => builder.MigrationsAssembly(typeof(Startup).Assembly.FullName)
                    )
                );

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
