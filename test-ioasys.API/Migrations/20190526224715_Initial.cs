﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace testioasys.API.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "EnterpriseType",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    enterprise_type_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EnterpriseType", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Portfolio",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    enterprises_number = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portfolio", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "enterprises",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    own_enterprise = table.Column<bool>(nullable: false),
                    enterprise_name = table.Column<string>(nullable: true),
                    photo = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    email_enterprise = table.Column<string>(nullable: true),
                    facebook = table.Column<string>(nullable: true),
                    twitter = table.Column<string>(nullable: true),
                    linkedin = table.Column<string>(nullable: true),
                    phone = table.Column<string>(nullable: true),
                    city = table.Column<string>(nullable: true),
                    country = table.Column<string>(nullable: true),
                    value = table.Column<int>(nullable: false),
                    share_price = table.Column<int>(nullable: false),
                    enterpriseTypeId = table.Column<int>(nullable: false),
                    portfolioId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_enterprises", x => x.id);
                    table.ForeignKey(
                        name: "FK_enterprises_EnterpriseType_enterpriseTypeId",
                        column: x => x.enterpriseTypeId,
                        principalTable: "EnterpriseType",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_enterprises_Portfolio_portfolioId",
                        column: x => x.portfolioId,
                        principalTable: "Portfolio",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Investor",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    investor_name = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    city = table.Column<string>(nullable: true),
                    country = table.Column<string>(nullable: true),
                    balance = table.Column<int>(nullable: false),
                    photo = table.Column<string>(nullable: true),
                    portfolioId = table.Column<int>(nullable: false),
                    porfolio_value = table.Column<int>(nullable: false),
                    first_access = table.Column<bool>(nullable: false),
                    super_angel = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Investor", x => x.id);
                    table.ForeignKey(
                        name: "FK_Investor_Portfolio_portfolioId",
                        column: x => x.portfolioId,
                        principalTable: "Portfolio",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_enterprises_enterpriseTypeId",
                table: "enterprises",
                column: "enterpriseTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_enterprises_portfolioId",
                table: "enterprises",
                column: "portfolioId");

            migrationBuilder.CreateIndex(
                name: "IX_Investor_portfolioId",
                table: "Investor",
                column: "portfolioId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "enterprises");

            migrationBuilder.DropTable(
                name: "Investor");

            migrationBuilder.DropTable(
                name: "EnterpriseType");

            migrationBuilder.DropTable(
                name: "Portfolio");
        }
    }
}
