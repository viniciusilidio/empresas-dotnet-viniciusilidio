﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using test_ioasys.Model.DatabaseSetup;

namespace testioasys.API.Migrations
{
    [DbContext(typeof(TestIoasysContext))]
    [Migration("20190526224715_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.4-servicing-10062")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("test_ioasys.Model.Entities.Enterprise", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("city");

                    b.Property<string>("country");

                    b.Property<string>("description");

                    b.Property<string>("email_enterprise");

                    b.Property<int>("enterpriseTypeId");

                    b.Property<string>("enterprise_name");

                    b.Property<string>("facebook");

                    b.Property<string>("linkedin");

                    b.Property<bool>("own_enterprise");

                    b.Property<string>("phone");

                    b.Property<string>("photo");

                    b.Property<int?>("portfolioId");

                    b.Property<int>("share_price");

                    b.Property<string>("twitter");

                    b.Property<int>("value");

                    b.HasKey("id");

                    b.HasIndex("enterpriseTypeId");

                    b.HasIndex("portfolioId");

                    b.ToTable("enterprises");
                });

            modelBuilder.Entity("test_ioasys.Model.Entities.EnterpriseType", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("enterprise_type_name");

                    b.HasKey("id");

                    b.ToTable("EnterpriseType");
                });

            modelBuilder.Entity("test_ioasys.Model.Entities.Investor", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("balance");

                    b.Property<string>("city");

                    b.Property<string>("country");

                    b.Property<string>("email");

                    b.Property<bool>("first_access");

                    b.Property<string>("investor_name");

                    b.Property<string>("photo");

                    b.Property<int>("porfolio_value");

                    b.Property<int>("portfolioId");

                    b.Property<bool>("super_angel");

                    b.HasKey("id");

                    b.HasIndex("portfolioId");

                    b.ToTable("Investor");
                });

            modelBuilder.Entity("test_ioasys.Model.Entities.Portfolio", b =>
                {
                    b.Property<int>("id")
                        .ValueGeneratedOnAdd()
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("enterprises_number");

                    b.HasKey("id");

                    b.ToTable("Portfolio");
                });

            modelBuilder.Entity("test_ioasys.Model.Entities.Enterprise", b =>
                {
                    b.HasOne("test_ioasys.Model.Entities.EnterpriseType", "enterprise_type")
                        .WithMany("enterprises")
                        .HasForeignKey("enterpriseTypeId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("test_ioasys.Model.Entities.Portfolio", "portfolio")
                        .WithMany("enterprises")
                        .HasForeignKey("portfolioId");
                });

            modelBuilder.Entity("test_ioasys.Model.Entities.Investor", b =>
                {
                    b.HasOne("test_ioasys.Model.Entities.Portfolio", "portfolio")
                        .WithMany()
                        .HasForeignKey("portfolioId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
