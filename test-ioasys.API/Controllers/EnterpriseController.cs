﻿using Microsoft.AspNetCore.Mvc;
using test_ioasys.API.Beans;
using test_ioasys.Model.Entities;
using test_ioasys.Service.Interfaces;

namespace test_ioasys.API.Controllers
{
    [Route("api/v1/enterprises")]
    [ApiController]
    public class EnterpriseController : ControllerBase
    {
        private readonly IEnterpriseService enterpriseService;

        public EnterpriseController(IEnterpriseService enterpriseService)
        {
            this.enterpriseService = enterpriseService;
        }

        [HttpPost]
        public IActionResult Post([FromBody]Enterprise enterprise)
        {
            enterpriseService.Create(enterprise);
            return Ok();
        }

        [HttpGet]
        public IActionResult Get([FromQuery]int enterprise_types, [FromQuery]string name)
        {
            if (enterprise_types == 0)
            {
                return Ok(new EnterpriseGetBean(enterpriseService.Get()));
            } else
            {
                return Ok(new EnterpriseGetBean(enterpriseService.Get(enterprise_types, name)));
            }
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            return Ok(new EnterpriseGetByIdBean(enterpriseService.Get(id)));
        }
    }
}
