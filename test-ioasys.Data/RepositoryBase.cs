﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using test_ioasys.Data.Interfaces;
using test_ioasys.Model.DatabaseSetup;
using test_ioasys.Model.Interfaces;

namespace test_ioasys.Data
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected TestIoasysContext Context;

        public RepositoryBase(TestIoasysContext context)
        {
            this.Context = context;
        }

        public DbSet<T> DbSet { get; set; }

        public T FindById<T>(int id) where T : class, IEntity
        {
            return this.Context.Set<T>().First(t => t.id == id);
        }

        public IQueryable<T> FindAll()
        {
            return this.Context.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.Context.Set<T>().Where(expression).AsNoTracking();
        }

        public void Create(T entity)
        {
            this.Context.Set<T>().Add(entity);
            this.Context.SaveChanges();
        }

        public void CreateSeveral(List<T> entities)
        {
            this.Context.Set<T>().AddRange(entities);
        }

        public void Update(T entity)
        {
            this.Context.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.Context.Set<T>().Remove(entity);
        }
    }
}
