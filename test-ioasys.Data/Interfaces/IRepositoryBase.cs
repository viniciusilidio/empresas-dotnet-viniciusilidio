﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using test_ioasys.Model.Interfaces;

namespace test_ioasys.Data.Interfaces
{
    public interface IRepositoryBase <T>
    {
        IQueryable<T> FindAll();

        IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression);

        T FindById<T>(int id) where T : class, IEntity;

        void Create(T entity);

        void CreateSeveral(List<T> entities);

        void Update(T entity);

        void Delete(T entity);
    }
}
