﻿using test_ioasys.Model.Entities;

namespace test_ioasys.Data.Interfaces
{
    public interface IEnterpriseRepository : IRepositoryBase<Enterprise>
    {
        Enterprise GetWithTypeById(int id);
    }
}
