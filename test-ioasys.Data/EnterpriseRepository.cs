﻿using Microsoft.EntityFrameworkCore;
using test_ioasys.Data.Interfaces;
using test_ioasys.Model.DatabaseSetup;
using test_ioasys.Model.Entities;

namespace test_ioasys.Data
{
    public class EnterpriseRepository : RepositoryBase<Enterprise>, IEnterpriseRepository
    {
        public EnterpriseRepository(TestIoasysContext context) : base(context)
        {
        }

        public Enterprise GetWithTypeById(int id)
        {
            return this.Context.enterprises.Include(x => x.enterprise_type).FirstOrDefaultAsync(e => e.id == id).Result;
        }
    }
}
