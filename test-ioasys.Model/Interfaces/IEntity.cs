﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test_ioasys.Model.Interfaces
{
    public interface IEntity
    {
        int id { get; }
    }
}
