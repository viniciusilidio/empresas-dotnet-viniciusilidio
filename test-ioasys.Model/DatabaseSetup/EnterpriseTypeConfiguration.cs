﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using test_ioasys.Model.Entities;

namespace test_ioasys.Model.DatabaseSetup
{
    class EnterpriseTypeConfiguration : IEntityTypeConfiguration<EnterpriseType>
    {
        public void Configure(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.HasKey(i => i.id);
            builder.Property(i => i.id).ValueGeneratedOnAdd();
        }
    }
}
