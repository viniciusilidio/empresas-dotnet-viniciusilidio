﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using test_ioasys.Model.Entities;

namespace test_ioasys.Model.DatabaseSetup
{
    class InvestorConfiguration : IEntityTypeConfiguration<Investor>
    {
        public void Configure(EntityTypeBuilder<Investor> builder)
        {
            builder.HasKey(i => i.id);
            builder.Property(i => i.id).ValueGeneratedOnAdd();
            builder.HasOne(i => i.portfolio);
        }
    }
}
