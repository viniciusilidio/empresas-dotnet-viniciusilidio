﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using test_ioasys.Model.Entities;

namespace test_ioasys.Model.DatabaseSetup
{
    class EnterpriseConfiguration : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.HasKey(e => e.id);
            builder.Property(e => e.id).ValueGeneratedOnAdd();
            builder.HasOne(e => e.enterprise_type);
            builder.HasOne(e => e.portfolio).WithMany(p => p.enterprises).HasForeignKey(x => x.portfolioId).IsRequired(false);
        }
    }
}
