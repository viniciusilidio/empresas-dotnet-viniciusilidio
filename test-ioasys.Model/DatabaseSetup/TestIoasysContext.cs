﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using test_ioasys.Model.Entities;

namespace test_ioasys.Model.DatabaseSetup
{
    public class TestIoasysContext : DbContext
    {
        public TestIoasysContext(DbContextOptions<TestIoasysContext> options) : base(options)
        { 
        }

        public DbSet<Enterprise> enterprises { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration(new EnterpriseConfiguration());
            builder.ApplyConfiguration(new EnterpriseTypeConfiguration());
            builder.ApplyConfiguration(new PortfolioConfiguration());
            builder.ApplyConfiguration(new InvestorConfiguration());
            base.OnModelCreating(builder);
        }
    }
}
