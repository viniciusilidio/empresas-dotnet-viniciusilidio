﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using test_ioasys.Model.Entities;

namespace test_ioasys.Model.DatabaseSetup
{
    class PortfolioConfiguration : IEntityTypeConfiguration<Portfolio>
    {
        public void Configure(EntityTypeBuilder<Portfolio> builder)
        {
            builder.HasKey(i => i.id);
            builder.Property(i => i.id).ValueGeneratedOnAdd();
            builder.HasMany(i => i.enterprises);
        }
    }
}
