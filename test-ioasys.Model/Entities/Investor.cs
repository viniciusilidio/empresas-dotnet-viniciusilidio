﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test_ioasys.Model.Entities
{
    public class Investor
    {
        public int id { get; set; }
        public string investor_name { get; set; }
        public string email { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public int balance { get; set; }
        public string photo { get; set; }
        public virtual Portfolio portfolio { get; set; }
        public int portfolioId { get; set; }
        public int porfolio_value { get; set; }
        public bool first_access { get; set; }
        public bool super_angel { get; set; }
    }
}
