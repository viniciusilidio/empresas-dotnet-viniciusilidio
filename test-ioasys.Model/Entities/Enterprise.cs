﻿using Microsoft.EntityFrameworkCore.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using test_ioasys.Model.Interfaces;

namespace test_ioasys.Model.Entities
{
    public class Enterprise : IEntity
    {
        public int id { get; set; }

        public bool own_enterprise { get; set; }

        public string enterprise_name { get; set; }

        public string photo { get; set; }

        public string description { get; set; }

        public string email_enterprise { get; set; }

        public string facebook { get; set; }

        public string twitter { get; set; }

        public string linkedin { get; set; }

        public string phone { get; set; }

        public string city { get; set; }

        public string country { get; set; }

        public int value { get; set; }

        public int share_price { get; set; }

        public virtual EnterpriseType enterprise_type { get; set; }

        [JsonIgnore]
        public int enterpriseTypeId { get; set; }

        [JsonIgnore]
        public virtual Portfolio portfolio { get; set; }

        [JsonIgnore]
        public int? portfolioId { get; set; }
    }
}
