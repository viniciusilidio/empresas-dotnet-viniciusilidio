﻿using System;
using System.Collections.Generic;
using System.Text;

namespace test_ioasys.Model.Entities
{
    public class Portfolio
    {
        public int id { get; set; }
        public int enterprises_number { get; set; }
        public List<Enterprise> enterprises { get; set; }
    }
}
