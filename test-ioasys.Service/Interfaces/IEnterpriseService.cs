﻿using System.Linq;
using System.Threading.Tasks;
using test_ioasys.Model.Entities;

namespace test_ioasys.Service.Interfaces
{
    public interface IEnterpriseService
    {
        void Create(Enterprise investor);
        IQueryable<Enterprise> Get();
        IQueryable<Enterprise> Get(int enterprise_types, string name);
        Enterprise Get(int id);
    }
}
