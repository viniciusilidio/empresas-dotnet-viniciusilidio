﻿using System.Linq;
using test_ioasys.Data.Interfaces;
using test_ioasys.Model.Entities;
using test_ioasys.Service.Interfaces;

namespace test_ioasys.Service
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IEnterpriseRepository enterpriseRepository;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository)
        {
            this.enterpriseRepository = enterpriseRepository;
        }

        public void Create(Enterprise investor)
        {
            enterpriseRepository.Create(investor);
        }

        public IQueryable<Enterprise> Get()
        {
            return enterpriseRepository.FindAll();
        }

        public IQueryable<Enterprise> Get(int enterprise_types, string name)
        {
            return enterpriseRepository.FindByCondition(e => e.enterpriseTypeId == enterprise_types && e.enterprise_name.Contains(name));
        }

        public Enterprise Get(int id)
        {
            return enterpriseRepository.GetWithTypeById(id);
        }
    }
}
